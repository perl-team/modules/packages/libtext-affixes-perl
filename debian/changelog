libtext-affixes-perl (0.09-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtext-affixes-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 19:27:22 +0100

libtext-affixes-perl (0.09-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Alejandro Garrido Mota from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 19:50:42 +0100

libtext-affixes-perl (0.09-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:46:16 +0200

libtext-affixes-perl (0.09-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Switch to source format "3.0 (quilt)".
  * Update years of upstream copyright.
  * Add debian/upstream/metadata.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.
  * Bump debhelper compatibility level to 9.
  * debian/rules: explicitly set perl_build buildsystem.
    The spurious Makefile confused dh(1).

 -- gregor herrmann <gregoa@debian.org>  Sun, 01 Nov 2015 19:06:52 +0100

libtext-affixes-perl (0.07-1) unstable; urgency=low

  * Initial Release (Closes: #538314).

 -- Alejandro Garrido Mota <garridomota@gmail.com>  Fri, 24 Jul 2009 13:22:23 -0430
